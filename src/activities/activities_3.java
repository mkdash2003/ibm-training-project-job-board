package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_3 {
WebDriver driver;
	@BeforeClass
	public void beforeTest() throws Exception {
	System.setProperty("webdriver.gecko.driver", "C://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();	
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/");
	}  
	@Test
	public void headerImage() throws Exception {
	WebElement img = driver.findElement(By.cssSelector(".attachment-large"));
	//Get the image URL
	String imgurl = img.getAttribute("src");
	//Print the URL
	System.out.println("Header Image URL: "+ imgurl);
	}
	@AfterClass
	public void browserClose() {
	//Close the Browser
	driver.close();
	}
}
		

	 