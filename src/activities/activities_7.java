package activities;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_7 {
WebDriver driver;
File file = new File("C:\\MANOJ\\Manoj_1\\Testing\\Selenium\\Manoj IBM SeleniumTrainning\\upload\\Logo7.png");

  @BeforeClass
  public void BrowserOpen() throws Exception {
	  System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
	  driver = new FirefoxDriver();
	  //Open Browser
	  driver.get("https://alchemy.hguy.co/jobs"); 
	  driver.manage().window().maximize();
  }
  @Test
  public void Preview() {
	  //Open Navigation menu
	  driver.findElement(By.linkText("Post a Job")).click();
	  //Fill the details and preview
	  driver.findElement(By.id("create_account_email")).sendKeys("mdashtest@email.com");
	  driver.findElement(By.id("job_title")).sendKeys("Test Automation Job");
	  driver.findElement(By.id("job_location")).sendKeys("Kolkata");
	  //Select a JobType from Dropdown
	  Select JobDropdown = new Select(driver.findElement(By.id("job_type")));
	  // Select an item by index
	  JobDropdown.selectByIndex(1);
	  //Fill Description
	  WebElement Desc = driver.findElement(By.cssSelector("#job_description_ifr"));
	  Desc.sendKeys(" Effective leadership and motivational skills.");
	  driver.findElement(By.cssSelector("#application")).sendKeys("jobs@email.com");
	  //Fill Company Details
	  driver.findElement(By.id("company_name")).sendKeys("IBM India Pvt ltd");
	  driver.findElement(By.id("company_website")).sendKeys("www.job.ibm.com");
	  driver.findElement(By.id("company_tagline")).sendKeys("Think");
	  driver.findElement(By.id("company_video")).sendKeys("shorturl.at/fDIW5");
	  driver.findElement(By.id("company_twitter")).sendKeys("@Test_IBM");
	  //Upload Logo
	  WebElement browseinput = driver.findElement(By.cssSelector("#company_logo"));
	  browseinput.sendKeys(file.getAbsolutePath());
	  //Preview Submit
	  driver.findElement(By.cssSelector("input.button:nth-child(4)")).click(); 
  }
  @Test
  public void SubmitListing() {
	  String url = driver.getCurrentUrl();
	  if (url.contains("https://alchemy.hguy.co/jobs/post-a-job/")) {
	  //Submit Listing
	  driver.findElement(By.cssSelector("#job_preview_submit_button")).click();
	  Reporter.log("Job submitted successfully.");
	  //Success Message
      String SuccessMessage = driver.findElement(By.cssSelector(".entry-content")).getText();
      System.out.println(SuccessMessage);
      Assert.assertEquals(SuccessMessage, "Job submitted successfully. Your listing will be visible once approved.");
	  }	
	  else {
		  Reporter.log("Listing page not found");
	  }
  }
  @AfterClass (alwaysRun = true)
     public void browseclose() {
	 driver.close();
 }
}
