package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_5 {
WebDriver driver;	
	@BeforeClass
	public void beforeTest() throws Exception {
	System.setProperty("webdriver.gecko.driver", "C://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/");
	}  
	@Test
	public void navigationbar() throws Exception {	
	// Find the navigation menu 'Jobs' and click
	driver.findElement(By.linkText("Jobs")).click();
	// Find the Page Title
	String pageTitle = driver.getTitle();
	System.out.println("Page Title : " + pageTitle);
	//Compare with the Expected
	Assert.assertEquals(pageTitle, "Jobs � Alchemy Jobs");
	}
	@AfterClass
	public void browserClose() {
	//Close the Browser
	driver.close();
	}
}
		