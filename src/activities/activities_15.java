package activities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_15 {
	WebDriver driver;	
	@BeforeClass (alwaysRun = true)
	public void browser() throws Exception {
	System.setProperty("webdriver.gecko.driver", "C://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/");
	}  
	@Test 
	public void jobsearch() throws Exception {	
	// Navigate to Job page
	driver.navigate().to("https://alchemy.hguy.co/jobs/jobs/");
	//Search for a Job
	driver.findElement(By.id("search_keywords")).sendKeys("Manager");
	driver.findElement(By.cssSelector(".search_submit > input:nth-child(1)")).click();
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	//Select A job
	driver.findElement(By.cssSelector(".post-34 > a:nth-child(1) > div:nth-child(2) > h3:nth-child(1)")).click();
	//Apply for Job
	driver.findElement(By.cssSelector(".application_button")).click();
	//Print the Email
	String emailAddress = driver.findElement(By.cssSelector(".job_application_email")).getText();
	System.out.println("Email Address :" + emailAddress);
	}
	@AfterClass (alwaysRun = true)
	public void browserClose() {
	//Close the Browser
	driver.close();
	}
}
		