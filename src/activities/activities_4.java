package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_4 {
	WebDriver driver;
	@BeforeClass
	public void beforeTest() throws Exception {
	System.setProperty("webdriver.gecko.driver", "C://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();	
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/");
	}  
	@Test
	public void secondHeading() throws Exception {
	WebElement secondheading = driver.findElement(By.cssSelector(".entry-title"));
	//Get the 2nd Heading 
	String htext = secondheading.getText();
	//Print the Second Heading
	System.out.println("Second Heading: " + htext);
	//Compare with Expected
	Assert.assertEquals(htext, "Quia quis non");
	}
	@AfterClass
	public void browserClose() {
	//Close the Browser
	driver.close();
	}
}
		