//Searching for jobs using XPath

package activities;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_11 {
WebDriver driver;
    @BeforeClass
    public void BrowseOpen() {
	 System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
	 driver = new FirefoxDriver();
	 //Open Job Page
	 driver.get("https://alchemy.hguy.co/jobs/"); 
	 driver.manage().window().maximize(); 
    }
    @Test
	public void SearchListing() {
    //Search keyword
    driver.findElement(By.linkText("Jobs")).click();
    driver.findElement(By.id("search_keywords")).sendKeys("Manager");
    
	// Filter Job Type
	driver.findElement(By.xpath("//*[@id=\"job_type_freelance\"]")).click();   //Freelance
	driver.findElement(By.xpath("//*[@id=\"job_type_internship\"]")).click();  //Internship
	driver.findElement(By.xpath("//*[@id=\"job_type_part-time\"]")).click();   //Disable Part Time
	driver.findElement(By.xpath("//*[@id=\"job_type_temporary\"]")).click();   //Disable Temporary
	
	// Search Jobs
	driver.findElement(By.cssSelector(".search_submit > input:nth-child(1)")).click();
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	
	//Open Job Listing
	WebElement listing = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/ul/li[4]/a"));
	listing.click();
	
	//System.out.println("Title is: " + driver.getTitle());
	String title = driver.findElement(By.xpath("//div[contains(@class,'ast-single-post-order')]/h1")).getText();
	//Print Listing Title
	System.out.println("Applied Job Title: " + title);
	
	//Apply Job
    driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/div/div/div[3]/input")).click();
    }
    @AfterClass(alwaysRun = true)
    public void browseclose() {
	 driver.close();
    }
 }
