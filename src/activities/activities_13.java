package activities;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class activities_13 {
WebDriver driver;
CSVReader reader = null;
File file = new File("C:\\Users\\MANOJDASH\\eclipse-workspace\\Batch-2-JOB-BOARD\\src\\activities\\activities_13");

	  @BeforeClass (alwaysRun = true)
	  public void BrowserOpen() throws Exception {
		  System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
		  driver = new FirefoxDriver();
		  //Open Browser
		  driver.get("https://alchemy.hguy.co/jobs"); 
		  driver.manage().window().maximize();
	  }
	  @Test
	  public void PostingJobs() throws IOException, CsvException {
		  //Open Navigation menu
		  driver.findElement(By.linkText("Post a Job")).click();
		  //Fill the details from CSV file
		  
		  reader = new CSVReader(new FileReader(file));
		  //Load content into list
	      List<String[]> list = reader.readAll();
	      System.out.println("Total number of rows are: " + list.size());
		  
	      //Create Iterator reference
		  Iterator<String[]> iterator = list.iterator();
		  iterator.next();
		  //Iterate all values
		  while(iterator.hasNext()) {
		  String[] str = iterator.next();
	    
		  String Youremail = str [0];
   		  String JobTitle = str [1];
		  String Location = str [2];
		  String Jobtype = str [3];
		  String desc = str [4];
		  String siteurl = str [5];
		  String companyname = str [6];
		  String website = str [7];
		    
		  //Fill the details
		  driver.findElement(By.id("create_account_email")).clear(); 
		  driver.findElement(By.id("create_account_email")).sendKeys(Youremail); //Email
		  driver.findElement(By.id("job_title")).clear();  
		  driver.findElement(By.id("job_title")).sendKeys(JobTitle);              //JobTitle
		  driver.findElement(By.id("job_location")).clear();  
		  driver.findElement(By.id("job_location")).sendKeys(Location); //Location
			  
		  //JobType from Dropdown
		  Select JobDropdown = new Select(driver.findElement(By.id("job_type")));   //Jobtype
		  JobDropdown.selectByVisibleText(Jobtype);
		  
		  //driver.findElement(By.cssSelector("#job_description_ifr")).clear();
		  driver.findElement(By.cssSelector("#job_description_ifr")).sendKeys(desc); //Desc
	      driver.findElement(By.cssSelector("#application")).clear();
		  driver.findElement(By.cssSelector("#application")).sendKeys(siteurl);//URL
		  driver.findElement(By.id("company_name")).clear();
		  driver.findElement(By.id("company_name")).sendKeys(companyname);//Companyname
		  driver.findElement(By.id("company_website")).clear(); 
		  driver.findElement(By.id("company_website")).sendKeys(website);//Website
			  
		  //Preview Submit
		  driver.findElement(By.cssSelector("input.button:nth-child(4)")).click();
		  //Submit Listing
		  //driver.findElement(By.cssSelector("#job_preview_submit_button")).click();
		  driver.findElement(By.id("job_preview_submit_button")).click();
		  driver.navigate().to("https://alchemy.hguy.co/jobs/post-a-job/?new=1&key=5e4431361c692");
		  driver.findElement(By.cssSelector("a.button")).click();
		  Reporter.log("Job submitted successfully.");
		  }
		 driver.close();
	     } 
         }
