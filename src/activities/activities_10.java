package activities;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_10 {
WebDriver driver;
    @BeforeClass
    public void BrowseOpen() {
	System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/wp-admin"); 
	driver.manage().window().maximize(); 
    }
    @Test
	public void AddNewUser() {
	//Login
	driver.findElement(By.id("user_login")).sendKeys("root");
	driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	driver.findElement(By.id("wp-submit")).click();
	//Click on User
    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[11]/a")).click();
    //Add New user
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/a")).click();
    //Fill Details
    driver.findElement(By.xpath("//*[@id=\"user_login\"]")).sendKeys("mktest92"); 		//User Name
    driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("mktest92@email.com");   //Email
    driver.findElement(By.xpath("//*[@id=\"first_name\"]")).sendKeys("mktest92");  		//First Name
    driver.findElement(By.xpath("//*[@id=\"last_name\"]")).sendKeys("dastest92"); 		//Last Name
    driver.findElement(By.xpath("//*[@id=\"url\"]")).sendKeys("www.gmail.com");		//Website
    //Click on Password Button
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/table/tbody/tr[6]/td/button")).click();
    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
    //Enter Password
    JavascriptExecutor js = (JavascriptExecutor) driver; 
    //((JavascriptExecutor) driver).executeScript("document.getElementById('pass1').setAttribute('value', 'Codingselenium22')");
    js.executeScript("document.getElementById('pass1').setAttribute('value', 'Codingselenium92')");
    //User notification unchecked
    driver.findElement(By.xpath("//*[@id=\"send_user_notification\"]")).click();    
    //Select Role
    Select Roledropdown = new Select(driver.findElement(By.xpath("//*[@id=\"role\"]")));
    Roledropdown.selectByIndex(5);
    //Add New user
    driver.findElement(By.xpath("//*[@id=\"createusersub\"]")).click();
    String successmsg = driver.findElement(By.id("message")).getText();
    //Assert user created message
    Assert.assertTrue(successmsg.contains("New user created."),"New user created.");
    System.out.println("New user created.");
    }
    @AfterClass(alwaysRun = true)
    public void browseclose() {
	driver.close();
}
}


