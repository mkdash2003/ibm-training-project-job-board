//Verify the website title
//Goal: Read the title of the website and verify the text

package activities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_1 {
WebDriver driver;
	
	@BeforeClass
	public void beforeTest() throws Exception {
	System.setProperty("webdriver.gecko.driver", "C://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/");
	}  
	@Test
	public void pageTitle() throws Exception {
	//Get the Title
	String pagetitle = driver.getTitle();
	System.out.println("Page Title : " + pagetitle);
	//Compare with Expected Title
	Assert.assertEquals(pagetitle, "Alchemy Jobs � Job Board Application");
	}
	@AfterClass
	public void browserClose() {
	//Close the Browser
	driver.close();
	}
}
		

	 