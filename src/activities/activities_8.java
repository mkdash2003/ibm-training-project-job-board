package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_8 {
	WebDriver driver;
	String heading;
	  @BeforeClass
	  public void BrowserOpen() throws Exception {
		  System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
		  driver = new FirefoxDriver();
		  //Open Browser
		  driver.get("https://alchemy.hguy.co/jobs/wp-admin"); 
		  driver.manage().window().maximize(); 
	  }
	  @Test
	  public void Login() {
		  //Login
		  driver.get("https://alchemy.hguy.co/jobs/wp-login.php");
		  driver.findElement(By.id("user_login")).sendKeys("root");
		  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		  driver.findElement(By.id("wp-submit")).click(); 
		  //WebElement heading = driver.findElement(By.xpath(".wrap"));
		  //heading.getText();
		  String pagetitle = driver.getTitle();
		  System.out.println("Page Title : " + pagetitle);
		  //Assert Logged In
		  Assert.assertEquals(pagetitle, "Dashboard � Alchemy Jobs � WordPress");
		  Reporter.log("Login Successful");
	  }
	  @AfterClass(alwaysRun = true)
	     public void browseclose() {
		 driver.close();
	 }
}
