package activities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class activities_14 {
WebDriver driver;
String successmsg;
JavascriptExecutor js;
    @BeforeClass
    public void BrowseOpen() {
	System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/wp-admin"); 
	driver.manage().window().maximize(); 
    }
    
    @DataProvider(name = "Authentication")
    public static Object[][] credentials() {
    return new Object[][] { { "root", "pa$$w0rd" }};
    }
 
    @Test (dataProvider = "Authentication")
	public void login(String username, String password) {
	//Enter User and Password
	WebElement usr = driver.findElement(By.id("user_login"));
	usr.sendKeys(username);
	WebElement pas = driver.findElement(By.id("user_pass"));
	pas.sendKeys(password);
	//Click Login
	driver.findElement(By.id("wp-submit")).click();
	}
    
	@Test (dependsOnMethods = "login")
	public void AddNewUser() throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	//Click on User
    driver.findElement(By.cssSelector("a.menu-icon-users")).click();
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/a")).click();
    //Fill Details
    driver.findElement(By.xpath("//*[@id=\"user_login\"]")).sendKeys("mktestm935"); 		//User Name
    driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("mktest935@email.com");   //Email
    driver.findElement(By.xpath("//*[@id=\"first_name\"]")).sendKeys("mktest935");  		//First Name
    driver.findElement(By.xpath("//*[@id=\"last_name\"]")).sendKeys("dastest925"); 		//Last Name
    driver.findElement(By.xpath("//*[@id=\"url\"]")).sendKeys("www.gmail.com");		//Website
    //Click on Password Button
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/table/tbody/tr[6]/td/button")).click();
    //Enter Password
    js = (JavascriptExecutor) driver; 
    //((JavascriptExecutor) driver).executeScript("document.getElementById('pass1').setAttribute('value', 'Codingselenium927')");
    js.executeScript("document.getElementById('pass1').setAttribute('value', 'Codingselenium935')");
    Thread.sleep(3000);
    //User notification unchecked
    driver.findElement(By.xpath("//*[@id=\"send_user_notification\"]")).click();    
    //Select Role
    Select Roledropdown = new Select(driver.findElement(By.xpath("//*[@id=\"role\"]")));
    Roledropdown.selectByIndex(4);
    //Add New user
    WebElement newuser = driver.findElement(By.cssSelector("#createusersub"));
    newuser.click();
    WebElement successmsg = driver.findElement(By.id("message"));
    String msg = successmsg.getText();
    //Assert user created message
    Assert.assertTrue(msg.contains("New user created."),"New user created.");
    Reporter.log("New user created.");
    }
    @AfterClass(alwaysRun = true)
    public void browseclose() {
	driver.close();
	Reporter.log("Test Completed");
}
}


