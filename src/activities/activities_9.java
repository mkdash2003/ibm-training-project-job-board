package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_9 {
WebDriver driver;
     @BeforeClass
     public void BrowseOpen() {
	 System.setProperty("webdriver.gecko.driver", "c://firefoxdriver//geckodriver.exe");
	 driver = new FirefoxDriver();
	 //Open Browser
	 driver.get("https://alchemy.hguy.co/jobs/wp-admin"); 
	 driver.manage().window().maximize(); 
     }
     @Test
	 public void CreateNewListing() {
	 //Login
	 driver.findElement(By.id("user_login")).sendKeys("root");
	 driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	 driver.findElement(By.id("wp-submit")).click();
     //Open Job Listing
     driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[7]/a")).click();
     //Open Add New
     driver.findElement(By.cssSelector(".page-title-action")).click();
     Reporter.log("New Listing Page is Opened");
     }
     @Test
     public void Publish() throws InterruptedException {
     driver.findElement(By.id("post-title-0")).click();
     driver.findElement(By.cssSelector(".nux-dot-tip__disable > svg:nth-child(1)")).click();
     //Fill the Form
     driver.findElement(By.id("post-title-0")).sendKeys("QA Manager Amazan");
     driver.findElement(By.id("_company_website")).sendKeys("https://www.ibm.com");
     driver.findElement(By.id("_filled")).click();
     driver.findElement(By.cssSelector("#_job_location")).sendKeys("Kolkata");
     driver.findElement(By.id("_company_name")).sendKeys("Amazan");
     driver.findElement(By.cssSelector("#job_listing_type-2 > label:nth-child(1)")).click();
     //Publish
     driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div/div[1]/div[2]/button[2]")).click(); 
     driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div/div[3]/div/div/div[1]/div/button")).click();
     Reporter.log("Job Posted Successfully"); 
     } 
     @AfterClass(alwaysRun = true)
     public void browseclose() {
	 driver.close();
     }
  }
