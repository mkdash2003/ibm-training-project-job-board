package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class activities_2 {
WebDriver driver;
	@BeforeClass
	public void beforeTest() throws Exception {
	System.setProperty("webdriver.gecko.driver", "C://firefoxdriver//geckodriver.exe");
	driver = new FirefoxDriver();
	//Open Browser
	driver.get("https://alchemy.hguy.co/jobs/");
	}  
	@Test
	public void headingTest() throws Exception {
	WebElement heading = driver.findElement(By.cssSelector(".entry-title"));
	//Get the Page Heading
	String pageheading = heading.getText();
	System.out.println("Page Heading: " + pageheading);
	//Compare with Expected Heading
	Assert.assertEquals(pageheading, "Welcome to Alchemy Jobs");
	}
	@AfterClass
	public void browserClose() {
	//Close the Browser
	driver.close();
	}
}
		

	 